// TODO: Implement proper error handling!

const PATH: &str = "/sys/class/backlight";

#[derive(Debug, PartialEq)]
enum Action {
	Increase,
	Decrease,
	Exact,
}

#[derive(Debug)]
struct Change {
	action: Action,
	how_much: f32, // Can be a fraction, which we then round
}

impl From<String> for Change {
	fn from(s: String) -> Self {
		let (s, action): (String, Action) = match s.chars().next() {
			Some('+') => (s[1..].to_string(), Action::Increase),
			Some('-') => (s[1..].to_string(), Action::Decrease),
			Some(_) => (s, Action::Exact),
			None => panic!("Problem"),
		};
		Self {
			action,
			how_much: s.parse().unwrap(),
		}
	}
}

fn main() {
	let mut args = std::env::args();
	if args.len() != 2 {
		print_usage();
	}
	let change: Change = args.nth(1).unwrap().into();

	let max = max_brightness();
	let mut new_value = change.how_much / 100.0 * max;
	new_value = match change.action {
		Action::Exact => new_value,
		Action::Increase => current_brightness() + new_value,
		Action::Decrease => current_brightness() - new_value,
	};
	new_value = new_value.min(max).max(0.0);

	write_brightness(new_value as i32);
}

fn print_usage() {
	eprintln!("Usage: brightness [+/-]N");
	std::process::exit(2);
}

/// Recturn the directory containing backlight kernel files. We expect there to only be one
/// directory within `PATH`, hence the use of `.last()` and `.unwrap()`.
fn acpi_path() -> std::path::PathBuf {
	// TODO: Lazy static this
	let dirs = std::fs::read_dir(PATH).unwrap();
	// TODO: Check dir contents first!
	dirs.last().unwrap().unwrap().path()
}

fn read_brightness(filename: &str, default: f32) -> f32 {
	let path = acpi_path().join(filename);
	let s = std::fs::read_to_string(path).unwrap();
	match s.trim().parse() {
		Ok(n) => n,
		Err(msg) => {
			eprintln!("{} {}", s, msg);
			default
		}
	}
}

fn write_brightness(value: i32) {
	let path = acpi_path().join("brightness");
	std::fs::write(path, format!("{}", value)).unwrap();
}

fn max_brightness() -> f32 {
	read_brightness("max_brightness", 7500.0)
}

fn current_brightness() -> f32 {
	read_brightness("brightness", 1000.0)
}
